﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpItem : MonoBehaviour {

    public string ObjectName = "";

    // Use this for initialization
    void Start()
    {
        ObjectName = gameObject.name;
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnMouseDown()
    {
        gameObject.SetActive(false);

        if (ObjectName == "Rock")
        {
            Inventory.RockActive = false;
            Inventory.LastObject = "Rock";
            Inventory.PlayerHasRock = true;
        }
        if (ObjectName == "Log")
        {
            Inventory.LogActive = false;
            Inventory.LastObject = "Log";
            Inventory.PlayerHasLog = true;
        }
    }
}
