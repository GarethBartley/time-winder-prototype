﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;

public class DialogueScript : MonoBehaviour
{
    [SerializeField]
    Canvas messageCanvas;

    public GameObject text;
    public GameObject image;
    public GameObject TextBox;


    void Start()
    {
        messageCanvas.enabled = false;
    }
 
    void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log("Touching Pessy");

        messageCanvas.enabled = true;

    }
    void OnTriggerExit2D(Collider2D other)
    {

        messageCanvas.enabled = false;

    }

}