﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectInventoryItem : MonoBehaviour
{

    public string InventorySlot = "";
    public static string selectedSlot = "";

    public GameObject SelectedSlot1;
    public GameObject SelectedSlot2;
    public GameObject SelectedSlot3;
    public GameObject SelectedSlot4;
    public GameObject SelectedSlot5;

    // Use this for initialization
    void Start()
    {



    }

    // Update is called once per frame
    void Update()
    {

        InventorySlot = gameObject.name;
    }

    void OnMouseDown()
    {
        if (InventorySlot == "Inventory1")
        {
            Inventory.selectedItem = "Inventory1";
            selectedSlot = "SelectedSlot1";

            SelectedSlot2.SetActive(false);
            SelectedSlot3.SetActive(false);
            SelectedSlot4.SetActive(false);
            SelectedSlot5.SetActive(false);
        }
        if (InventorySlot == "Inventory2")
        {
            Inventory.selectedItem = "Inventory2";
            selectedSlot = "SelectedSlot2";

            SelectedSlot1.SetActive(false);
            SelectedSlot3.SetActive(false);
            SelectedSlot4.SetActive(false);
            SelectedSlot5.SetActive(false);
        }
        if (InventorySlot == "Inventory3")
        {
            Inventory.selectedItem = "Inventory3";
            selectedSlot = "SelectedSlot3";

            SelectedSlot1.SetActive(false);
            SelectedSlot2.SetActive(false);
            SelectedSlot4.SetActive(false);
            SelectedSlot5.SetActive(false);
        }
        if (InventorySlot == "Inventory4")
        {
            Inventory.selectedItem = "Inventory4";
            selectedSlot = "SelectedSlot4";

            SelectedSlot1.SetActive(false);
            SelectedSlot2.SetActive(false);
            SelectedSlot3.SetActive(false);
            SelectedSlot5.SetActive(false);
        }
        if (InventorySlot == "Inventory5")
        {
            Inventory.selectedItem = "Inventory5";
            selectedSlot = "SelectedSlot5";

            SelectedSlot1.SetActive(false);
            SelectedSlot2.SetActive(false);
            SelectedSlot3.SetActive(false);
            SelectedSlot4.SetActive(false);
        }

    }
}
