﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

    public float playerSpeed = 4.0f;

    private float currentSpeed;

    private Animator animator;

    // Use this for initialization
    void Start () {

        animator = GetComponent<Animator>();

    }
	
	// Update is called once per frame
	void Update () {

        updatePosition();

	}

    void updatePosition()
    {

        animator.SetBool("right", false);
        animator.SetBool("left", false);
        animator.SetBool("down", false);
        animator.SetBool("up", false);

        if (Input.GetKey(KeyCode.D))
        {
            animator.SetBool("right", true);
            transform.position += Vector3.right * playerSpeed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.A))
        {
            animator.SetBool("left", true);
            transform.position += Vector3.left * playerSpeed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.W))
        {
            animator.SetBool("up", true);
            transform.position += Vector3.up * playerSpeed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.S))
        {
            animator.SetBool("down", true);
            transform.position += Vector3.down * playerSpeed * Time.deltaTime;
        }

        else
        {
            animator.SetBool("idle", true);
        }
    }
}
