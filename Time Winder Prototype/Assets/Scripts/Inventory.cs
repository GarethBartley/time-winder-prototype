﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Inventory : MonoBehaviour
{

    public static string currentItem = "none";
    public static string selectedItem = "none";
    public static string ItemInSelectedSlot = "";
    public static string LastObject = "";

    public static bool[] InventoryArray = { false, false, false, false, false };
    public static string[] InventoryItemNameArray = { "", "", "", "", "" };

    public GameObject Inventory1;
    public GameObject Inventory2;
    public GameObject Inventory3;
    public GameObject Inventory4;
    public GameObject Inventory5;

    public GameObject SelectedSlot1;
    public GameObject SelectedSlot2;
    public GameObject SelectedSlot3;
    public GameObject SelectedSlot4;
    public GameObject SelectedSlot5;

    public Sprite InventorySlot;
    public Sprite RockSprite;
    public Sprite LogSprite;

    public GameObject Log;
    public GameObject Rock;
    
    public bool Inventory1SpriteChanged = false;
    public bool Inventory2SpriteChanged = false;

    public static bool Item1InInventory = false;
    public static bool Item2InInventory = false;

    public static int CurrentElement = 0;

    public static bool RockActive = true;
    public static bool LogActive = true;

    public static bool PlayerHasRock = false;
    public static bool PlayerHasLog = false;

    // Use this for initialization
    void Start()
    {
        Inventory1.SetActive(false);
        Inventory2.SetActive(false);
        Inventory3.SetActive(false);
        Inventory4.SetActive(false);
        Inventory5.SetActive(false);

        SelectedSlot1.SetActive(false);
        SelectedSlot2.SetActive(false);
        SelectedSlot3.SetActive(false);
        SelectedSlot4.SetActive(false);
        SelectedSlot5.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {

        updateInventory();

        if (Item1InInventory == false)
        {
            if (RockActive == false)
            {
                findCurrentElement();
                InventoryArray[CurrentElement] = true;
                currentItem = "Rock";
                InventoryItemNameArray[CurrentElement] = currentItem;
                Debug.Log(InventoryItemNameArray[CurrentElement]);
                Item1InInventory = true;
            }
        }

        if (Item2InInventory == false)
        {
            if (LogActive == false)
            {
                findCurrentElement();
                InventoryArray[CurrentElement] = true;
                currentItem = "Log";
                InventoryItemNameArray[CurrentElement] = currentItem;
                Debug.Log(InventoryItemNameArray[CurrentElement]);
                Item2InInventory = true;
            }
        }

        if (selectedItem == "Inventory1")
        {            
            ItemInSelectedSlot = InventoryItemNameArray[0];
        }
        if (selectedItem == "Inventory2")
        {
            ItemInSelectedSlot = InventoryItemNameArray[1];
        }

        if (SelectInventoryItem.selectedSlot == "SelectedSlot1")
        {
            SelectedSlot1.SetActive(true);
        }
        if (SelectInventoryItem.selectedSlot == "SelectedSlot2")
        {
            SelectedSlot2.SetActive(true);
        }
        if (SelectInventoryItem.selectedSlot == "SelectedSlot3")
        {
            SelectedSlot3.SetActive(true);
        }
        if (SelectInventoryItem.selectedSlot == "SelectedSlot4")
        {
            SelectedSlot4.SetActive(true);
        }
        if (SelectInventoryItem.selectedSlot == "SelectedSlot5")
        {
            SelectedSlot5.SetActive(true);
        }
    }

    void updateInventory()
    {
        if (InventoryArray[0] == true)
        {
            Inventory1.SetActive(true);
            if (Inventory1SpriteChanged == false)
            {
                if (LastObject == "Rock")
                {
                    GameObject.Find("Inventory1").GetComponent<SpriteRenderer>().sprite = RockSprite;
                    Inventory1SpriteChanged = true;
                }
                if (LastObject == "Log")
                {
                    GameObject.Find("Inventory1").GetComponent<SpriteRenderer>().sprite = LogSprite;
                    Inventory1SpriteChanged = true;
                }
            }

        }
        if (InventoryArray[1] == true)
        {
            Inventory2.SetActive(true);
            if (Inventory2SpriteChanged == false)
            {
                if (LastObject == "Rock")
                {
                    GameObject.Find("Inventory2").GetComponent<SpriteRenderer>().sprite = RockSprite;
                    Inventory2SpriteChanged = true;
                }
                if (LastObject == "Log")
                {
                    GameObject.Find("Inventory2").GetComponent<SpriteRenderer>().sprite = LogSprite;
                    Inventory2SpriteChanged = true;
                }
            }
        }
    }

    void findCurrentElement()
    {
        for (int i = 0; i < InventoryArray.Length; i++)
        {
            if (InventoryArray[i] == false)
            {
                CurrentElement = i;
                return;
            }
        }
    }
}


