﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectPuzzleItem : MonoBehaviour
{

    public Sprite PuzzleCompleted;
    public Sprite RockSprite;
    public Sprite LogSprite;

    public GameObject PuzzleItem;
    public GameObject InventorySlot;
    public GameObject SelectedSlot;

    public GameObject Inventory1;
    public GameObject Inventory2;
    public GameObject Inventory3;
    public GameObject Inventory4;
    public GameObject Inventory5;

    public int ItemElement;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        SelectedSlot = GameObject.Find(SelectInventoryItem.selectedSlot);
    }

    void OnMouseDown()
    {
        if (Inventory.PlayerHasLog == true && Inventory.PlayerHasRock == true)
        {
            GameObject.Find("PuzzleItem").GetComponent<SpriteRenderer>().sprite = PuzzleCompleted;

            Inventory1.SetActive(false);
            Inventory2.SetActive(false);
            Inventory3.SetActive(false);
            Inventory4.SetActive(false);
            Inventory5.SetActive(false);

            Inventory.InventoryArray[0] = false;
            Inventory.InventoryArray[1] = false;
            Inventory.InventoryArray[2] = false;
            Inventory.InventoryArray[3] = false;
            Inventory.InventoryArray[4] = false;

            Inventory.InventoryItemNameArray[0] = "";
            Inventory.InventoryItemNameArray[1] = "";
            Inventory.InventoryItemNameArray[2] = "";
            Inventory.InventoryItemNameArray[3] = "";
            Inventory.InventoryItemNameArray[4] = "";
        }


        
        //if (Inventory.ItemInSelectedSlot == "Rock")
        //{
        //    GameObject.Find("PuzzleItem").GetComponent<SpriteRenderer>().sprite = PuzzleCompleted;
        //    InventorySlot = GameObject.Find(Inventory.selectedItem);
        //    InventorySlot.SetActive(false);            
        //    SelectedSlot.SetActive(false);
        //    ItemElement = Array.IndexOf(Inventory.InventoryItemNameArray, "Rock");
        //    Inventory.InventoryArray[ItemElement] = false;
        //    switch (ItemElement)
        //    {
        //        case 0:
        //            Inventory.Item1InInventory = false;
        //            Inventory.RockActive = true;
        //            break;
        //        case 1:
        //            Inventory.Item2InInventory = false;
        //            Inventory.LogActive = true;
        //            break;
        //    }
        //    Inventory.CurrentElement = 0;
        //    Inventory.selectedItem = "none";
        //    SelectInventoryItem.selectedSlot = "";
        //}
        //else
        //{
        //    if (Inventory.ItemInSelectedSlot == "Log")
        //    {
        //        Inventory.selectedItem = "none";
        //        SelectedSlot.SetActive(false);
        //        SelectInventoryItem.selectedSlot = "";
        //    }
        //}
    }
}
